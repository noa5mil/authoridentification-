the goal of the scripts is to Predict the author of a given text from books by:
    Edgar Allan Poe, Howard Phillips Lovecraft, Mary Wollstonecraft Shelley
by using pamiliar Python NLP tools

there are 4 different scripts that with 4 different algorithms to achive the identification:

1.MaxAppearance - 
    For each author we created a dictionary containing all of his written words from the train set and the 
    number of times they appeared overall. For this we used a Stop Word list and stemmed the words to avoid overfitting.
    The prediction step: in each sample we determined the Autor that had the most appearances for each word.
    The error is about 33% - which is an improvement from random labeling (which is 66%).

2+3.laplac && goodTuringEsimator - 
    based on Markov model with 1 gram.
    For each author we created a dictionary containing all his written words from the train set and their probability of their appearance.
    We also used a Stop Word list and stemmed the words to avoid overfitting.
    We encountered never before seen words in the test set so We solved that by using 2 different types of estimators:
    Laplace Law and Good Turing Estimator
    The prediction step: for each sample we calculated the Maximum Likelihood from the authors’ models.
    The error with Laplace Law is about 18% and with Good Turing it is about 17%.

4.2Order - 
    based on Markov model with 2 gram.
    For each author we created a vector with the probabilities of the first word of the sentence and a 2D matrix containing the probability 
    of 2 sequential words.
    We stemmed the words to avoid overfitting.
    there was not enough training data so the matrix and vector were very sparse and the smoothing process wasn’t enough to achieve a good predictor. 
    So the error was almost as bad as using a random predictor.
